module.exports = {
  devServer: {
    proxy: {
      '/api/*': {
        target: 'http://localhost:8090',
        ws: true,
        prependPath: true,
        changeOrigin: true,
        secure: false,
        pathRewrite: { '^/api': '' }
      }
    }
  },
  "transpileDependencies": [
    "vuetify"
  ]
}