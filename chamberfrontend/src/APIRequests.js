import firebase from "firebase";


const getRequest = async (url) => {
    const token = await firebase.auth().currentUser.getIdToken();
    return fetch(`/api/${url}`, {
        method: "GET",
        headers: new Headers({ 'Authorization': `Bearer ${token}`, 'Content-type': 'application/json' }),
    }).then((response) => {
        return (!response.ok ?  Promise.reject(response.status): response.json());
    });
};

const postRequest = async (url, data) => {
    const token = await firebase.auth().currentUser.getIdToken();
    return fetch(`/api/${url}`, {
        method: "POST",
        headers: new Headers({ 'Authorization': `Bearer ${token}`, 'Content-type': 'application/json' }),
        body: JSON.stringify(data)
    }).then((response) => {
        return (!response.ok ?  Promise.reject(response.status): response.json());
    });
};

// const putRequest = async (url, data) => {
//     const token = await firebase.auth().currentUser.getIdToken();
//     return fetch(`/api/${url}`, {
//         method: "PUT",
//         headers: new Headers({ 'Authorization': `Bearer ${token}`, 'Content-type': 'application/json' }),
//         body: JSON.stringify(data)
//     }).then((response) => {
//         return (!response.ok ?  Promise.reject(response.status): response);
//     });
// };

// const deleteRequest = async (url, data) => {
//     const token = await firebase.auth().currentUser.getIdToken();
//     return fetch(`/api/${url}`, {
//         method: "DELETE",
//         headers: new Headers({ 'Authorization': `Bearer ${token}`, 'Content-type': 'application/json' }),
//         body: JSON.stringify(data)
//     }).then((response) => {
//         return (!response.ok ?  Promise.reject(response.status): response);
//     });
// };

export const getCategoryById = (data) => {
    return getRequest('category/' + data)
};

export const getVotableById = (votableId) => {
    return getRequest('/api/votable/'+votableId)
}

export const getAllCategories = () => {
    return getRequest('/api/category')
}

export const postComment = (comment, votableId) => {
    return postRequest('api/comment/' + votableId, comment);
}


