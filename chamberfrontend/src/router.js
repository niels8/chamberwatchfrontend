import VueRouter from 'vue-router'
// import firebase from "firebase";
import Frontpage from "./components/Frontpage";
import Votable from "./components/Votable";
import LogIn from "./components/LogIn";
const routes = [
    {
        path: '',
        redirect: '/login'
    },
    {
        path: '/frontpage',
        component: Frontpage,
    },
    {
        path: '/votable/:votableId',
        component: Votable,
        props: true,
    },
    {
        path: '/login',
        component: LogIn
    }

];

let router = new VueRouter({
    mode: 'history',
    routes
});

// router.beforeEach(async(to, from, next) => {
//     let user =  await firebase.auth().currentUser;
//     if (to.matched.some(record => record.meta.authRequired)) {
//         if (!user) {
//             next({
//                 path: '/login',
//             })
//         } else {
//             next()
//         }
//     } else if(to.matched.some(record => record.meta.guest)){
//         if (user) {
//             next({
//                 path: '/home',
//             })
//         } else {
//             next()
//         }
//     }
//     else{
//         next()
//     }
// });
export default router
