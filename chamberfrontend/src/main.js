import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import firebase from "firebase"
import {firebaseConfig} from "./firebaseConfig"
import VueSocketIO from 'vue-socket.io'
import VueRouter from "vue-router"
import router from "./router";
// import FrontPage from './components/FrontPage'
// import VueSocketIO from 'vue-socket.io';

// const routes = {
//   '/' : FrontPage,
  
// }
Vue.config.productionTip = false
Vue.use(VueRouter);
firebase.initializeApp(firebaseConfig);
let app = null;
firebase.auth().onAuthStateChanged( async (user) => {
  if(user){
    Vue.use(new VueSocketIO({
      debug: true,
      connection: 'http://localhost:3000',
      options: {query: {token: await firebase.auth().currentUser.getIdToken()}}
    }));
  }
  if(!app){
    app = new Vue({
      render: h => h(App),
      vuetify,
      router
    }).$mount('#app');
  }

});

